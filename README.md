# Idf Uart Hello World

I use UART1 to send "Hello, World!" in an infinite loop, to read it with an external UART adapter, I configured it to use GPIO7 for tx and GPIO6 for rx.

## Before you can start
You need esp-idf setted up for this to work.

If you have it already all you need to do is setting the correct target.
For example:
```
idf.py set-target esp32c3
```

and then you can build it using `idf.py.build`

For further information look at the official [ESP32 Getting Started Guide](https://docs.espressif.com/projects/esp-idf/en/stable/get-started/index.html)