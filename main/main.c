#include "driver/uart.h"
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define BUF_SIZE (1024)

// 21 and 20 are the standard uart GPIO pins, but they are used by UART 0
static int GPIO_TX = 7;
static int GPIO_RX = 6;

void uart_init() {
    uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
    };
    uart_param_config(UART_NUM_1, &uart_config);
    uart_set_pin(UART_NUM_1, GPIO_TX, GPIO_RX, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    uart_driver_install(UART_NUM_1, BUF_SIZE * 2, BUF_SIZE * 2, 0, NULL, 0);
}

void uart_write(const char* msg) {
    uart_write_bytes(UART_NUM_1, msg, strlen(msg));
}

void app_main(void)
{
    uart_init();
    const char* message = "Hello, World!";
    while(1){
        uart_write(message);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
}
